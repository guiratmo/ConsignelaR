﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consignelaWpf.Model
{
    class Resultat
    {
        [JsonProperty("Code_Participant")]
        public String Code_Participant { get; set; }
        [JsonProperty("momentJour")]
        public List<MomentJour> momentJour { get; set; }
    }
}
