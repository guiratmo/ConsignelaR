﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace consignelaWpf.Model
{
    public class Consigne
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("description")]
        public String description { get; set; }
        [JsonProperty("img_path")]
        public String img_path { get; set; }
        //[JsonProperty("imgBitmap")]
        //public BitmapImage imgBitmap { get; set; }
        [JsonProperty("imgNom")]
        public String imgNom { get; set; }

    }
}
