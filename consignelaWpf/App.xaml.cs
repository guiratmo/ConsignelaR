﻿using consignelaWpf.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using static System.Environment;

namespace consignelaWpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static ListConsignes consignesSelect = new ListConsignes();
        public static Prescription prescriptionSelect = new Prescription();
        public static PrescriptionTab prescriptionTabSelect = new PrescriptionTab();
        public static String experimentateur = "";
        public static String date = "";
        public static String type = "";
        public static String utilisation = "";
        public static String pilulier = "";
        public static String format = "";
        public static String mode = "";
        public static String navigation = "";


        public static bool expSansAUthen = false;
        public static bool authentification = false;
        public static String codeParticipant;
        public static string logIn = "";
        public static string password = "";
        public static string jsonFromCheckAccess;
        //public static StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
        //public static StorageFolder imageFolder;
        public static string txtresponseLogin;
        public static bool verifDataPage = false;
        //public static SpecialFolder storageFolder = SpecialFolder.LocalApplicationData;
        
    }
}
