﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace consignelaWpf.pages
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        private Stopwatch _stopwatch;
        private int _lastElapsedMs;

        public MainPage()
        {
            InitializeComponent();
            //timer();
            StartTimer();
        }
        public void StartTimer()
        {
            _lastElapsedMs = 0;
            _stopwatch = Stopwatch.StartNew();
        }
        private void keypress(object sender, RoutedEventArgs e)
        {
            int elapsedMs = (int)_stopwatch.ElapsedMilliseconds;
            //int elapsedS = (int)_stopwatch.Elapsed.Seconds;
            //int currentElapsed = (elapsedMs - _lastElapsedMs);

            //_lastElapsedMs = elapsedMs;

            System.Diagnostics.Debug.WriteLine(elapsedMs);
            //label1.contents = currentElapsed.ToString();
        }
    
        public void timer()
        {
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            dispatcherTimer.Start();

        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            int mSec = 0;
            mSec += dispatcherTimer.Interval.Milliseconds;
            System.Diagnostics.Debug.WriteLine(mSec);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

      
    }
}
