﻿using consignelaWpf.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Diagnostics;

namespace consignelaWpf.pages
{
    /// <summary>
    /// Interaction logic for Pilulier.xaml
    /// </summary>
    public partial class Pilulier : System.Windows.Controls.Page
    {
        internal Medicament medicamentChoisi;
        internal Prescription prescriptionTmp;
        private Prescription prescriptionTmp2;
        internal List<Medicament> listMedicament = new List<Medicament>();
        internal List<Medicament> listMedicamentCloned = new List<Medicament>();
        internal List<ListView> gridViewTabMedicament = new List<ListView>();
        internal List<ListView> gridViewTabMedicamentAffiche = new List<ListView>();
        internal List<List<Medicament>> tabListMedicaments = new List<List<Medicament>>();
        internal List<Medicament> tabListMedicamentsAffich;
        internal List<int> nbrPilules = new List<int>();


        internal List<bool> open = new List<bool>();
        int j;
        bool singleTap,doubleTap, nvMedicament, mediSelected = false;
        internal String openCase = "";
        internal Medicament medicamentSelect;
        internal int indexMediSelect;
        internal int IndexgridviewSelect;
        Resultat resultatFinal;
        internal ExperiencePrescResult ExpPrescResulat;
        internal List<ExperiencePrescResult> ListExpPrescResulat = new List<ExperiencePrescResult>();
        internal List<ExperienceConf> ListExpConf = new List<ExperienceConf>();
        internal List<ExperienceConf> ListExperiencesConfTmp;
        public String sampleFileExp;
        string pathFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        String sampleFileExperienceConf;
        internal String strJsonExpConf;
        internal int indexExp;
        String storageDirectory, ExcelDirectory;
        internal PrescriptionTab prescriptionTab = App.prescriptionTabSelect;
        internal int i;
        private Stopwatch _stopwatch;
        private int elapsedMs;
        int nbrAffPresc = 1, nbrAffPil = 1;
        internal List<Etape> listEtapes = new List<Etape>();

        Microsoft.Office.Interop.Excel.Application excel;
        Microsoft.Office.Interop.Excel.Workbook worKbooK;
        Microsoft.Office.Interop.Excel.Worksheet worKsheeT;
        Microsoft.Office.Interop.Excel.Range celLrangE;

        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        


        public Pilulier()
        {
            InitializeComponent();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            
            chargerListMedicament();
            listMedicamentCloned = new List<Medicament>(listMedicament);
            //listMedicament.CopyTo(listMedicamentCloned);
            //listMedicamentCloned = listMedicament.Select(i => i.Clone()).ToList();
            gridViewBoiteMedicaments.ItemsSource = listMedicament;
            gridViewNomsMedicaments.ItemsSource = listMedicament;
            loadGridView();
            loadGridViewAffich();
            loadPrescription();
            loadTabListMedicaments();
            loadOpen();
            loadExperienceConf();
            loadPrescriptionVerbale();
            if (App.format == "tab")
            {
                prescriptionTabulaire.Visibility = Visibility.Visible;
                prescriptionVerbale.Visibility = Visibility.Collapsed;
            }
            else if(App.format == "verb")
            {
                prescriptionTabulaire.Visibility = Visibility.Collapsed;
                prescriptionVerbale.Visibility = Visibility.Visible;
            }
            AfficheDate.Text = "Date : "+App.date;

            _stopwatch = Stopwatch.StartNew();
            saveEvent("Appui boutonDEBUT", "Déclenchement du chronomètre & Affichage1 PRESCRIPTION", "");
            //exportToExcel();

            if (App.navigation == "activee")
                RetourPrescription.Visibility = Visibility.Visible;
            else if (App.navigation == "desactivee")
                RetourPrescription.Visibility = Visibility.Collapsed;



            storageDirectory = System.IO.Path.Combine(pathFolder, "Consignela");
            Directory.CreateDirectory(storageDirectory);
            sampleFileExp = System.IO.Path.Combine(storageDirectory, "ExperiencePrescResult.json");

            ExcelDirectory = System.IO.Path.Combine(storageDirectory, "Fichiers_Excel");
            Directory.CreateDirectory(ExcelDirectory);
        }
        public void saveEvent(String action, String codageEvent,String comment)
        {
            int tempsEcoule = (int)_stopwatch.ElapsedMilliseconds;
            Etape nouvelleEtape = new Etape { Temps_ecoule = tempsEcoule, Action = action, Codage_evenement = codageEvent };
            listEtapes.Add(nouvelleEtape);
            System.Diagnostics.Debug.WriteLine(nouvelleEtape.Temps_ecoule+" "+nouvelleEtape.Action+" "+nouvelleEtape.Codage_evenement);
        }
        public void loadPrescriptionVerbale()
        {
            ListViewPrescVerbale.ItemsSource = null;
            ListViewPrescVerbale.ItemsSource = listMedicament;
        }
        //public void timer()
        //{
        //    dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
        //    dispatcherTimer.Interval = new TimeSpan(0, 0, 0,0,1);
        //    dispatcherTimer.Start();

        //}
        //private void dispatcherTimer_Tick(object sender, EventArgs e)
        //{
        //    System.Diagnostics.Debug.WriteLine(dispatcherTimer.Interval.Milliseconds);
        //}
        public void loadExperienceConf()
        {
            var storageDirectory = System.IO.Path.Combine(pathFolder, "Consignela");

            sampleFileExperienceConf = System.IO.Path.Combine(storageDirectory, "ExperienceConf.json");

            //var ExperienceConf = File.Create(sampleFileExperienceConf);
            //ExperienceConf.Close();


            if (File.Exists(sampleFileExperienceConf))
            {
                strJsonExpConf = File.ReadAllText(sampleFileExperienceConf);
                ListExperiencesConfTmp = JsonConvert.DeserializeObject<List<ExperienceConf>>(strJsonExpConf);
            }

            if (ListExperiencesConfTmp != null)
            {
                indexExp = ListExperiencesConfTmp[ListExperiencesConfTmp.Count - 1].id + 1;
            }
            else
            {
                indexExp = 0;
            }
            //newListPrescriptions.Reverse();                
        }
        public void loadGridView()
        {
            gridViewTabMedicament.Add(gridViewTabMedicament11);
            gridViewTabMedicament.Add(gridViewTabMedicament12);
            gridViewTabMedicament.Add(gridViewTabMedicament13);
            gridViewTabMedicament.Add(gridViewTabMedicament14);
            gridViewTabMedicament.Add(gridViewTabMedicament15);
            gridViewTabMedicament.Add(gridViewTabMedicament16);
            gridViewTabMedicament.Add(gridViewTabMedicament17);
            gridViewTabMedicament.Add(gridViewTabMedicament21);
            gridViewTabMedicament.Add(gridViewTabMedicament22);
            gridViewTabMedicament.Add(gridViewTabMedicament23);
            gridViewTabMedicament.Add(gridViewTabMedicament24);
            gridViewTabMedicament.Add(gridViewTabMedicament25);
            gridViewTabMedicament.Add(gridViewTabMedicament26);
            gridViewTabMedicament.Add(gridViewTabMedicament27);
            gridViewTabMedicament.Add(gridViewTabMedicament31);
            gridViewTabMedicament.Add(gridViewTabMedicament32);
            gridViewTabMedicament.Add(gridViewTabMedicament33);
            gridViewTabMedicament.Add(gridViewTabMedicament34);
            gridViewTabMedicament.Add(gridViewTabMedicament35);
            gridViewTabMedicament.Add(gridViewTabMedicament36);
            gridViewTabMedicament.Add(gridViewTabMedicament37);
        }
        public void loadGridViewAffich()
        {
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage11);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage12);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage13);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage14);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage15);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage16);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage17);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage21);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage22);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage23);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage24);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage25);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage26);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage27);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage31);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage32);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage33);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage34);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage35);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage36);
            gridViewTabMedicamentAffiche.Add(gridViewTabMedicamentAffichage37);
        }
        public void loadPrescription()
        {
         
            listMedicament = prescriptionTmp.medicaments;
            foreach(Medicament medicament in listMedicament)
            {

                nbrPilules.Add(medicament.nbr);
                
            }
            //tbId.Text = prescriptionTmp.nom;
            if (prescriptionTmp != null)
            {
                listMedicament = prescriptionTmp.medicaments;


                gridViewNomsMedicaments.ItemsSource = null;
                gridViewNomsMedicaments.ItemsSource = listMedicament;


                for (int i = 0; i < 21; i++)
                {
                    if (prescriptionTab.momentJour[i].listMedicaments.Count != 0)
                    {
                        tabListMedicamentsAffich = prescriptionTab.momentJour[i].listMedicaments;
                        gridViewTabMedicamentAffiche[i].ItemsSource = null;
                        gridViewTabMedicamentAffiche[i].ItemsSource = tabListMedicamentsAffich;
                    }
                }
            }
        }
        public void loadTabListMedicaments()
        {
            for (int i = 0; i < 21; i++)
            {
                tabListMedicaments.Add(new List<Medicament>());
            }

        }
        public void loadOpen()
        {
            for (int i = 0; i < 21; i++)
            {
                open.Add(false);
            }
        }
        public void chargerListMedicament()
        {
            prescriptionTmp2 = App.prescriptionSelect;
            prescriptionTmp = prescriptionTmp2;


            foreach (Medicament medicament in prescriptionTmp.medicaments)
            {
                listMedicament.Add(medicament);
            }
        }

        private void RetourPrescription_Click(object sender, RoutedEventArgs e)
        {
            
            PilulierPanel.Visibility = Visibility.Collapsed;
            PrescriptonAffichage.Visibility = Visibility.Visible;
            saveEvent("Appui ButtonPrescritpion", "Affichage" + nbrAffPresc + "Prescription","");
            nbrAffPresc++;

        }
             

        private void exportToExcel()
        {
            var ExcelFile = System.IO.Path.Combine(pathFolder, "Consignela","Fichiers_Excel", App.codeParticipant);

            //sampleFile = System.IO.Path.Combine(ExcelDirectory, "ConsignesData.json");

            Microsoft.Office.Interop.Excel.Application xla = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb = xla.Workbooks.Add(XlSheetType.xlWorksheet);
            Worksheet ws = (Worksheet)xla.ActiveSheet;

            xla.Visible = false;
            ws.Cells[1, 1] = "Expérimentateur";
            ws.Cells[2, 1] = "Date";
            ws.Cells[3, 1] = "Code Participant";
            ws.Cells[4, 1] = "Type";
            ws.Cells[5, 1] = "Utilisation";
            ws.Cells[6, 1] = "Pilulier";
            ws.Cells[7, 1] = "Format";
            ws.Cells[8, 1] = "Mode";
            ws.Cells[9, 1] = "Navigation";


            ws.Cells[1,2] = App.experimentateur;
            ws.Cells[2, 2] = App.date;
            ws.Cells[3, 2] = App.codeParticipant;
            ws.Cells[4, 2] = App.type;
            ws.Cells[5, 2] = App.utilisation;
            ws.Cells[6, 2] = App.pilulier;
            ws.Cells[7, 2] = App.format;
            ws.Cells[8, 2] = App.mode;
            ws.Cells[9, 2] = App.navigation;

            Worksheet wsEvent = (Worksheet)xla.Worksheets.Add();

            wsEvent.Cells[1, 1] = "Temps Ecoule";
            wsEvent.Cells[1, 2] = "Action";
            wsEvent.Cells[1, 3] = "Codage evenement";


            Worksheet wsPilulier = (Worksheet)xla.Worksheets.Add();
            wsPilulier.Cells[2, 3] = "Lundi";
            wsPilulier.Cells[2, 4] = "Mardi";
            wsPilulier.Cells[2, 5] = "Mercredi";
            wsPilulier.Cells[2, 6] = "Jeudi";
            wsPilulier.Cells[2, 7] = "Vendredi";
            wsPilulier.Cells[2, 8] = "Samedi";
            wsPilulier.Cells[2, 9] = "Dimanche";

            wsPilulier.Cells[3, 2] = "Matin";
            wsPilulier.Cells[4, 2] = "Midi";
            wsPilulier.Cells[5, 2] = "Soir";
            wsPilulier.Cells[7, 2] = "Matin";
            wsPilulier.Cells[8, 2] = "Midi";
            wsPilulier.Cells[9, 2] = "Soir";
            wsPilulier.Cells[11, 2] = "Matin";
            wsPilulier.Cells[12, 2] = "Midi";
            wsPilulier.Cells[13, 2] = "Soir";
            wsPilulier.Cells[15, 2] = "Matin";
            wsPilulier.Cells[16, 2] = "Midi";
            wsPilulier.Cells[17, 2] = "Soir";




            //        wb.SaveAs(ExcelFile + ".xls", Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value,
            //Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange,
            //Excel.XlSaveConflictResolution.xlUserResolution, true,
            //Missing.Value, Missing.Value, Missing.Value);

            wb.SaveAs(ExcelFile+".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
            wb.Close();
            xla.Quit();

        }

        private void finirExp_Click(object sender, RoutedEventArgs e)
        {
            saveEvent("Appui boutonFIN", "Fin", "");
            int h = 0;
            resultatFinal = new Resultat();
            resultatFinal.Code_Participant = App.codeParticipant;
            resultatFinal.momentJour = new List<MomentJour>();

            for (int p = 0; p < 3; p++)
            {
                for (int k = 0; k < 7; k++)
                {
                    resultatFinal.momentJour.Add(new MomentJour { abs = p, coord = k, listMedicaments = tabListMedicaments[h] });
                    //resultatFinal.momentJour[k].abs = p;
                    //resultatFinal.momentJour[k].coord = k;
                    //resultatFinal.momentJour[k].listMedicaments = tabListMedicaments[h];
                    h++;
                }

            }

            ExpPrescResulat = new ExperiencePrescResult();
            ExpPrescResulat.experimentateur = App.experimentateur;
            ExpPrescResulat.date = App.date;
            ExpPrescResulat.Code_Participant = App.codeParticipant;
            ExpPrescResulat.type = App.type;
            ExpPrescResulat.utilisation = App.utilisation;
            ExpPrescResulat.pilulier = App.pilulier;
            ExpPrescResulat.format = App.format;
            ExpPrescResulat.mode = App.mode;
            ExpPrescResulat.navigation = App.navigation;
            ExpPrescResulat.etapes = listEtapes;
            ExpPrescResulat.prescription = App.prescriptionTabSelect;
            ExpPrescResulat.Resultat = resultatFinal;

            ListExpPrescResulat.Add(ExpPrescResulat);
            File.WriteAllText(sampleFileExp, JsonConvert.SerializeObject(ListExpPrescResulat));

            ListExpConf = new List<ExperienceConf>();
            if (ListExperiencesConfTmp != null)
            {
                ListExpConf = ListExperiencesConfTmp;
            }
            i = 0;
            foreach (Medicament medicament in prescriptionTmp.medicaments)
            {
                medicament.nbr = nbrPilules[i] ;
                i++;
            }
            ListExpConf.Add(new ExperienceConf { dateTime = DateTime.Today.Date,id = indexExp, listeConsigne = App.consignesSelect,codeParticipant = App.codeParticipant,prescription= prescriptionTmp , prescriptionTab=App.prescriptionTabSelect });
            File.WriteAllText(sampleFileExperienceConf, JsonConvert.SerializeObject(ListExpConf));

           // fichier Excel



            feedbackMsg.Text = "L'expérience '" + App.codeParticipant + "' a bien été enregistrée ";
            if (!feedback.IsOpen) feedback.IsOpen = true;
            //show(feedback);


        }
        public void show(Popup popup)
        {

            popup.IsOpen = true;

            DispatcherTimer timer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(2)
            };

            timer.Tick += delegate (object sender, EventArgs e)
            {
                ((DispatcherTimer)timer).Stop();
                if (popup.IsOpen) popup.IsOpen = false;
            };

            timer.Start();
            
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            if (medicamentSelect != null)
            {
                gridViewTabMedicament[IndexgridviewSelect].ItemsSource = null;
                gridViewTabMedicament[IndexgridviewSelect].ItemsSource = tabListMedicaments[IndexgridviewSelect];
                if (open[IndexgridviewSelect] == true && tabListMedicaments[IndexgridviewSelect][indexMediSelect].nbrTab > 0)
                {
                    
                    tabListMedicaments[IndexgridviewSelect][indexMediSelect].nbrTab--;
                    if(tabListMedicaments[IndexgridviewSelect][indexMediSelect].nbrTab == 0)
                    {
                        tabListMedicaments[IndexgridviewSelect].Remove(tabListMedicaments[IndexgridviewSelect][indexMediSelect]);
                    }
                    gridViewTabMedicament[IndexgridviewSelect].ItemsSource = null;
                    gridViewTabMedicament[IndexgridviewSelect].ItemsSource = tabListMedicaments[IndexgridviewSelect];
                    //listMedicament[j].nbr--;

                    for (int l = 0; l < listMedicament.Count; l++)
                    {
                        if (listMedicament[l].nom == medicamentSelect.nom)
                        {
                            listMedicament[l].nbr++;
                            break;
                        }
                    }
                    saveEvent("Appui CORBEILLE", "Suppression du médicament "+ medicamentSelect.nom, "");

                    gridViewBoiteMedicaments.ItemsSource = null;
                    gridViewBoiteMedicaments.ItemsSource = listMedicament;
                    
                }
                medicamentSelect = null;
            }
            else
                saveEvent("Appui CORBEILLE", "pas d’événement", "");

            gridViewBoiteMedicaments.ItemsSource = null;
            gridViewBoiteMedicaments.ItemsSource = listMedicament;
        }

      
      
        //private void gridViewBoiteMedicaments_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    medicamentChoisi = (Medicament)gridViewBoiteMedicaments.SelectedItem;
        //    j = gridViewBoiteMedicaments.SelectedIndex;

        //}
        private void gridViewBoiteMedicaments_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            medicamentChoisi = (Medicament)gridViewBoiteMedicaments.SelectedItem;
            j = gridViewBoiteMedicaments.SelectedIndex;
            saveEvent("Appui Boite-"+ medicamentChoisi.nom, "Sélection1 UNITE-" + medicamentChoisi.nom, "");

            medicamentSelect = null;
        }
        private void ExecSingleTap(int i, ListView gridviewTab, String codeAlveole)
        {
            nvMedicament = true;
            if (open[i] == true && listMedicament[j].nbr > 0 && medicamentChoisi != null)
            {
                //System.Diagnostics.Debug.WriteLine(tabListMedicaments[i].Count);
                //if there is this medecines
                for (int k = 0; k < tabListMedicaments[i].Count; k++)
                {
                    if (tabListMedicaments[i][k].nom == medicamentChoisi.nom)
                    {
                        tabListMedicaments[i][k].nbrTab++;
                        //medicamentChoisi.nbrTab = 0;
                        nvMedicament = false;
                        break;
                    }
                }
                if (nvMedicament)
                {
                    medicamentChoisi.nbrTab = 1;
                    tabListMedicaments[i].Add(medicamentChoisi);
                }
                listMedicament[j].nbr--;
                saveEvent("Appui ALVEOLE-" + codeAlveole, "Dépôt"+ medicamentChoisi.nbrTab+" UNITE -" + medicamentChoisi.nom, "");
                
                gridViewBoiteMedicaments.ItemsSource = null;
                gridViewBoiteMedicaments.ItemsSource = listMedicament;

                gridviewTab.ItemsSource = null;
                gridviewTab.ItemsSource = tabListMedicaments[i];

                medicamentChoisi = null;
            }
            else if (listMedicament[j].nbr <= 0)
            {
                saveEvent("Appui ALVEOLE-"+ codeAlveole, "pas d'évennement, la boite du médicament est vide ", "");
                //MessageDialog boiteVide = new MessageDialog("La boite de " + listMedicament[j].nom + " est vide");
                //boiteVide.ShowAsync();
            }
            else if (medicamentChoisi == null)
            {
                //MessageDialog selesctMedi = new MessageDialog("choisissez un médicament svp");
                //selesctMedi.ShowAsync();
            }
            else
                saveEvent("Appui ALVEOLE-" + codeAlveole, "pas d'évennement, l'alveole-"+ codeAlveole+" est fermée", "");

        }
        private void ExecDoubleTap(int i, ListView gridviewTab, String codeAlveole)
        {
            if (!open[i])
            {
                if (openCase == "")
                {
                    open[i] = true;
                    openCase = gridviewTab.Name;
                    gridviewTab.Background = new SolidColorBrush(Colors.White);
                    saveEvent("Appui ALVEOLE-"+ codeAlveole, "Ouverture ALVEOLE-"+codeAlveole, "");

                }
                else
                {
                    //MessageDialog autreCaseOuverte = new MessageDialog("fermer l'autre case ouverte d'abord");
                    //autreCaseOuverte.ShowAsync();
                }
            }
            else
            {
                open[i] = false;
                openCase = "";
                gridviewTab.Background = new SolidColorBrush(Colors.WhiteSmoke);
                saveEvent("Appui ALVEOLE-"+ codeAlveole, "Fermeture ALVEOLE-"+ codeAlveole, "");
            }

         
        }
        private void gridViewTabMedicament11Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(0, gridViewTabMedicament11, "M1J1");
                    if(gridViewTabMedicament11.SelectedItem !=null )
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 0;
                        medicamentSelect = (Medicament)gridViewTabMedicament11.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament11.Items.IndexOf(gridViewTabMedicament11.SelectedItem);
                    }
                   
                }
                else

                {
                    ExecDoubleTap(0, gridViewTabMedicament11, "M1J1");
                }
            }
        }
        private void gridViewTabMedicament12Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(1, gridViewTabMedicament12, "M1J2");
                    if (gridViewTabMedicament12.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 1;
                        medicamentSelect = (Medicament)gridViewTabMedicament12.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament12.Items.IndexOf(gridViewTabMedicament12.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(1, gridViewTabMedicament12, "M1J2");
                }
            }
        }
        private void gridViewTabMedicament13Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(2, gridViewTabMedicament13, "M1J3");
                    if (gridViewTabMedicament13.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 2;
                        medicamentSelect = (Medicament)gridViewTabMedicament13.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament13.Items.IndexOf(gridViewTabMedicament13.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(2, gridViewTabMedicament13, "M1J3");
                }
            }
        }
        private void gridViewTabMedicament14Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(3, gridViewTabMedicament14, "M1J4");
                    if (gridViewTabMedicament14.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 3;
                        medicamentSelect = (Medicament)gridViewTabMedicament14.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament14.Items.IndexOf(gridViewTabMedicament14.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(3, gridViewTabMedicament14, "M1J4");
                }
            }
        }
        private void gridViewTabMedicament15Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(4, gridViewTabMedicament15, "M1J5");
                    if (gridViewTabMedicament15.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 4;
                        medicamentSelect = (Medicament)gridViewTabMedicament15.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament15.Items.IndexOf(gridViewTabMedicament15.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(4, gridViewTabMedicament15, "M1J5");
                }
            }
        }
        private void gridViewTabMedicament16Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(5, gridViewTabMedicament16, "M1J6");
                    if (gridViewTabMedicament16.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 5;
                        medicamentSelect = (Medicament)gridViewTabMedicament16.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament16.Items.IndexOf(gridViewTabMedicament16.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(5, gridViewTabMedicament16, "M1J6");
                }
            }
        }
        private void gridViewTabMedicament17Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(6, gridViewTabMedicament17, "M1J7");
                    if (gridViewTabMedicament17.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 6;
                        medicamentSelect = (Medicament)gridViewTabMedicament17.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament17.Items.IndexOf(gridViewTabMedicament17.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(6, gridViewTabMedicament17, "M1J7");
                }
            }
        }
        private void gridViewTabMedicament21Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(7, gridViewTabMedicament21, "M2J1");
                    if (gridViewTabMedicament21.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 7;
                        medicamentSelect = (Medicament)gridViewTabMedicament21.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament21.Items.IndexOf(gridViewTabMedicament21.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(7, gridViewTabMedicament21, "M2J1");
                }
            }
        }
        private void gridViewTabMedicament22Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(8, gridViewTabMedicament22, "M2J2");
                    if (gridViewTabMedicament22.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 8;
                        medicamentSelect = (Medicament)gridViewTabMedicament22.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament22.Items.IndexOf(gridViewTabMedicament22.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(8, gridViewTabMedicament22, "M2J2");
                }
            }
        }
        private void gridViewTabMedicament23Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(9, gridViewTabMedicament23, "M2J3");
                    if (gridViewTabMedicament23.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 9;
                        medicamentSelect = (Medicament)gridViewTabMedicament23.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament23.Items.IndexOf(gridViewTabMedicament23.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(9, gridViewTabMedicament23, "M2J3");
                }
            }
        }
        private void gridViewTabMedicament24Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(10, gridViewTabMedicament24, "M2J4");
                    if (gridViewTabMedicament24.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 10;
                        medicamentSelect = (Medicament)gridViewTabMedicament24.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament24.Items.IndexOf(gridViewTabMedicament24.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(10, gridViewTabMedicament24, "M2J4");
                }
            }
        }
        private void gridViewTabMedicament25Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(11, gridViewTabMedicament25, "M2J5");
                    if (gridViewTabMedicament25.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 11;
                        medicamentSelect = (Medicament)gridViewTabMedicament25.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament25.Items.IndexOf(gridViewTabMedicament25.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(11, gridViewTabMedicament25, "M2J5");
                }
            }
        }
        private void gridViewTabMedicament26Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(12, gridViewTabMedicament26, "M2J6");
                    if (gridViewTabMedicament26.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 12;
                        medicamentSelect = (Medicament)gridViewTabMedicament26.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament26.Items.IndexOf(gridViewTabMedicament26.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(12, gridViewTabMedicament26, "M2J6");
                }
            }
        }
        private void gridViewTabMedicament27Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(13, gridViewTabMedicament27, "M2J7");
                    if (gridViewTabMedicament27.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 13;
                        medicamentSelect = (Medicament)gridViewTabMedicament27.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament27.Items.IndexOf(gridViewTabMedicament27.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(13, gridViewTabMedicament27, "M2J7");
                }
            }
        }
        private void gridViewTabMedicament31Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(14, gridViewTabMedicament31, "M3J1");
                    if (gridViewTabMedicament31.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 14;
                        medicamentSelect = (Medicament)gridViewTabMedicament31.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament31.Items.IndexOf(gridViewTabMedicament31.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(14, gridViewTabMedicament31, "M3J1");
                }
            }
        }
        private void gridViewTabMedicament32Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(15, gridViewTabMedicament32, "M3J2");
                    if (gridViewTabMedicament32.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 15;
                        medicamentSelect = (Medicament)gridViewTabMedicament32.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament32.Items.IndexOf(gridViewTabMedicament32.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(15, gridViewTabMedicament32, "M3J2");
                }
            }
        }
        private void gridViewTabMedicament33Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(16, gridViewTabMedicament33, "M3J3");
                    if (gridViewTabMedicament33.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 16;
                        medicamentSelect = (Medicament)gridViewTabMedicament33.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament33.Items.IndexOf(gridViewTabMedicament33.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(16, gridViewTabMedicament33, "M3J3");
                }
            }
        }
        private void gridViewTabMedicament34Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(17, gridViewTabMedicament34, "M3J4");
                    if (gridViewTabMedicament34.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 17;
                        medicamentSelect = (Medicament)gridViewTabMedicament34.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament34.Items.IndexOf(gridViewTabMedicament34.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(17, gridViewTabMedicament34, "M3J4");
                }
            }
        }
        private void gridViewTabMedicament35Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(18, gridViewTabMedicament35, "M3J5");
                    if (gridViewTabMedicament35.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 18;
                        medicamentSelect = (Medicament)gridViewTabMedicament35.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament35.Items.IndexOf(gridViewTabMedicament35.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(18, gridViewTabMedicament35, "M3J5");
                }
            }
        }
        private void gridViewTabMedicament36Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(19, gridViewTabMedicament36, "M3J6");
                    if (gridViewTabMedicament36.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 19;
                        medicamentSelect = (Medicament)gridViewTabMedicament36.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament36.Items.IndexOf(gridViewTabMedicament36.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(19, gridViewTabMedicament36, "M3J6");
                }
            }
        }

       

        private void gridViewTabMedicament37Item_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(20, gridViewTabMedicament37, "M3J17");
                    if (gridViewTabMedicament37.SelectedItem != null)
                    {
                        mediSelected = true;
                        //System.Diagnostics.Debug.WriteLine((Medicament)gridViewTabMedicament11.SelectedItem);
                        IndexgridviewSelect = 20;
                        medicamentSelect = (Medicament)gridViewTabMedicament37.SelectedItem;
                        //System.Diagnostics.Debug.WriteLine(medicamentSelect.nom);
                        indexMediSelect = gridViewTabMedicament37.Items.IndexOf(gridViewTabMedicament37.SelectedItem);
                    }
                }
                else

                {
                    ExecDoubleTap(20, gridViewTabMedicament37, "M3J17");
                }
            }
        }

        private void gridViewTabMedicament11_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(0, gridViewTabMedicament11, "M1J1");

            }
            else

            {
                ExecDoubleTap(0, gridViewTabMedicament11, "M1J1");
            }
        }

        private void gridViewTabMedicament12_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(1, gridViewTabMedicament12, "M1J2");
            }
            else

            {
                ExecDoubleTap(1, gridViewTabMedicament12, "M1J2");
            }
        }
        private void gridViewTabMedicament13_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(2, gridViewTabMedicament13, "M1J3");
            }
            else

            {
                ExecDoubleTap(2, gridViewTabMedicament13, "M1J3");
            }
        }

        private void gridViewTabMedicament14_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(3, gridViewTabMedicament14, "M1J4");
            }
            else

            {
                ExecDoubleTap(3, gridViewTabMedicament14, "M1J4");
            }
        }

        private void gridViewTabMedicament15_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(4, gridViewTabMedicament15, "M1J5");
            }
            else

            {
                ExecDoubleTap(4, gridViewTabMedicament15, "M1J5");
            }
        }

        private void gridViewTabMedicament16_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(5, gridViewTabMedicament16, "M1J6");
            }
            else

            {
                ExecDoubleTap(5, gridViewTabMedicament16, "M1J6");
            }
        }

        private void gridViewTabMedicament17_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(6, gridViewTabMedicament17, "M1J7");
            }
            else

            {
                ExecDoubleTap(6, gridViewTabMedicament17, "M1J7");
            }
        }

        private void gridViewTabMedicament21_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(7, gridViewTabMedicament21, "M2J1");
            }
            else

            {
                ExecDoubleTap(7, gridViewTabMedicament21, "M2J1");
            }
        }

        private void gridViewTabMedicament22_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(8, gridViewTabMedicament22, "M2J2");
            }
            else

            {
                ExecDoubleTap(8, gridViewTabMedicament22, "M2J2");
            }
        }

        private void gridViewTabMedicament23_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(9, gridViewTabMedicament23, "M2J3");
            }
            else

            {
                ExecDoubleTap(9, gridViewTabMedicament23, "M2J3");
            }
        }

        private void gridViewTabMedicament24_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(10, gridViewTabMedicament24, "M2J4");
            }
            else

            {
                ExecDoubleTap(10, gridViewTabMedicament24, "M2J4");
            }
        }

        private void gridViewTabMedicament25_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(11, gridViewTabMedicament25, "M2J5");
            }
            else

            {
                ExecDoubleTap(11, gridViewTabMedicament25, "M2J5");
            }
        }

        private void gridViewTabMedicament26_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(12, gridViewTabMedicament26, "M2J6");
            }
            else

            {
                ExecDoubleTap(12, gridViewTabMedicament26, "M2J6");
            }
        }

        private void gridViewTabMedicament27_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(13, gridViewTabMedicament27, "M2J7");
            }
            else

            {
                ExecDoubleTap(13, gridViewTabMedicament27, "M2J7");
            }
        }

        private void gridViewTabMedicament31_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(14, gridViewTabMedicament31, "M3J1");
            }
            else

            {
                ExecDoubleTap(14, gridViewTabMedicament31, "M3J1");
            }
        }

        private void gridViewTabMedicament32_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(15, gridViewTabMedicament32, "M3J2");
            }
            else

            {
                ExecDoubleTap(15, gridViewTabMedicament32, "M3J2");
            }
        }

        private void gridViewTabMedicament33_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(16, gridViewTabMedicament33, "M3J3");
            }
            else

            {
                ExecDoubleTap(16, gridViewTabMedicament33, "M3J3");
            }
        }



        private void RetournerAcceuil_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pages/MainPage.xaml", UriKind.Relative));

        }

        private void PilulierBtn_Click(object sender, RoutedEventArgs e)
        {
            
            PrescriptonAffichage.Visibility = Visibility.Collapsed;
            PilulierPanel.Visibility = Visibility.Visible;
            saveEvent("AppuiButtonPilulier", "Affichage"+ nbrAffPil + "Pilulier","");
            nbrAffPil++;
        }

        private void gridViewTabMedicament34_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(17, gridViewTabMedicament34, "M3J4");
            }
            else

            {
                ExecDoubleTap(17, gridViewTabMedicament34, "M3J4");
            }
        }

        private void gridViewTabMedicament35_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(18, gridViewTabMedicament35, "M3J5");
            }
            else

            {
                ExecDoubleTap(18, gridViewTabMedicament35, "M3J5");
            }
        }

        private void gridViewTabMedicament36_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(19, gridViewTabMedicament36, "M3J6");
            }
            else

            {
                ExecDoubleTap(19, gridViewTabMedicament36, "M3J6");
            }
        }

        private void gridViewTabMedicament37_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(20, gridViewTabMedicament37, "M3J7");
            }
            else

            {
                ExecDoubleTap(20, gridViewTabMedicament37, "M3J7");
            }
        }

        


    }
}
