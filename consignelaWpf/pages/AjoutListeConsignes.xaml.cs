﻿using consignelaWpf.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace consignelaWpf.pages
{
    /// <summary>
    /// Interaction logic for AjoutListeConsignes.xaml
    /// </summary>
    public partial class AjoutListeConsignes : Page
    {
        internal static String imgPath = null;
        internal static String imgNom = null;
        internal static int i, j, k;
        private int index;
        internal List<ListConsignes> ListConsTemp;
        internal List<Consigne> consignes = new List<Consigne>();
        private ObservableCollection<Consigne> ListLignesConsignes = new ObservableCollection<Consigne>();
        private Collection<Consigne> listConsignetmp = new Collection<Consigne>();
        internal Consigne consigneTmp = new Consigne();
        //internal StorageFile sampleFile;
        byte[] bytes;
        string pathFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        String sampleFile;
        String sampleFileServeur;


        public AjoutListeConsignes()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            loadConsignesData();
        }

        private void loadConsignesData()
        {
            //imageFolder = await App.storageFolder.GetFolderAsync("images");
           
            var storageDirectory = System.IO.Path.Combine(pathFolder, "Consignela");
            Directory.CreateDirectory(storageDirectory);
            sampleFile = System.IO.Path.Combine(storageDirectory, "ConsignesData.json");

            if (File.Exists(sampleFile))
            {
                string str_Consigne_json = File.ReadAllText(sampleFile);
                ListConsTemp = JsonConvert.DeserializeObject<List<ListConsignes>>(str_Consigne_json);
             
                if (ListConsTemp.Count != 0)
                {
                    i = ListConsTemp[ListConsTemp.Count - 1].id + 1;
                }
                else
                {
                    i = 0;
                }
            }

        }
     

        private void btn_Ajout_img_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;

            System.Diagnostics.Debug.Write(listViewListConsigne.SelectedItem);
            System.Diagnostics.Debug.Write(consigneTmp);
            Microsoft.Win32.OpenFileDialog openPicker = new Microsoft.Win32.OpenFileDialog();
            openPicker.DefaultExt = ".jpg";
            openPicker.Filter = "Images |*.jpg;*.jpeg; *.png";

            //var picker = new Windows.Storage.Pickers.FileOpenPicker();
            //picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            //picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            //picker.FileTypeFilter.Add(".jpg");
            //picker.FileTypeFilter.Add(".jpeg");
            //picker.FileTypeFilter.Add(".png");
            Nullable<bool> result = openPicker.ShowDialog();

            if (result == true)
            {
                var imageBtm = new BitmapImage();
                imageBtm.UriSource = new Uri(openPicker.FileName, UriKind.RelativeOrAbsolute);
                //ListLignesConsignes[consigneTmp.id].imgBitmap = imageBtm;
                ListLignesConsignes[consigneTmp.id].img_path = openPicker.FileName;
                System.Diagnostics.Debug.WriteLine(openPicker.SafeFileName);
                ListLignesConsignes[consigneTmp.id].imgNom = openPicker.SafeFileName;

                chargerListConsignetmp();

            }

        }

        private void up_Tapped(object sender, RoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmp.IndexOf(consigneTmp);
            if (indexConsigne > 0)
            {
                listConsignetmp[indexConsigne].id--;
                listConsignetmp[indexConsigne - 1].id++;

                listConsignetmp[indexConsigne] = listConsignetmp[indexConsigne - 1];
                listConsignetmp[indexConsigne - 1] = consigneTmp;

                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmp;
            }
        }

        private void down_Tapped(object sender, RoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmp.IndexOf(consigneTmp);

            if (indexConsigne < listConsignetmp.Count() - 1)
            {
                listConsignetmp[indexConsigne].id++;
                listConsignetmp[indexConsigne + 1].id--;

                listConsignetmp[indexConsigne] = listConsignetmp[indexConsigne + 1];
                listConsignetmp[indexConsigne + 1] = consigneTmp;



                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmp;
            }
        }

        private void deleteAjout_Tapped(object sender, RoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            if (listConsignetmp.Count > 1)
            {

                listConsignetmp.Remove(consigneTmp);
                int indexC = 0;
                foreach (Consigne consigne in listConsignetmp)
                {
                    consigne.id = indexC;
                    indexC++;
                }
                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmp;

            }
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(nomListeConsignes.Text) && listConsignetmp.Count() > 1)
            {
                List<ListConsignes> listListConsignes = new List<ListConsignes>();
                Date date = new Date();
                date.day = DateTime.Today.Day.ToString();
                date.month = DateTime.Today.Month.ToString();
                date.month = date.monthToStr();
                date.year = DateTime.Today.Year.ToString();
                ListConsignes listConsigne = new ListConsignes { id = i, nom = nomListeConsignes.Text, date = DateTime.Today.Date.ToString("dd-MM-yyyy"), dateTime = DateTime.Today.Date, dateLettre = date };
                listConsigne.consignes = listConsignetmp.Cast<Consigne>().ToList();

                if (ListConsTemp != null)
                {
                    listListConsignes = ListConsTemp;
                }
                listListConsignes.Add(listConsigne);
                File.WriteAllText(sampleFile, JsonConvert.SerializeObject(listListConsignes));

                //var stream = await sampleFile.OpenStreamForReadAsync();
                //bytes = new byte[(int)stream.Length];
                //stream.Read(bytes, 0, (int)stream.Length);
                //fileToByte();
                //await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFile.Path);

                

                feedbackMsg.Text = "La consigne '"+ nomListeConsignes.Text + "' a bien été ajoutée ";
                show(feedback);

            }
            else
            {
                //MessageDialog erreurAjout = new MessageDialog("Veuillez vérifiez tous les champs s.v.p");
                //await erreurAjout.ShowAsync();

            }
        }

        public void show(Popup popup)
        {

            popup.IsOpen = true;

            DispatcherTimer timer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(2)
            };

            timer.Tick += delegate (object sender, EventArgs e)
            {
                ((DispatcherTimer)timer).Stop();
                if (popup.IsOpen) popup.IsOpen = false;
            };

            timer.Start();

        }

       

        private void listViewListConsigne_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            consigneTmp = (Consigne)(sender as ListView).SelectedItem;
         
        }

      
        private void ajoutImg_Click(object sender, RoutedEventArgs e)
        {
            if(consigneTmp != null)
            {
                System.Diagnostics.Debug.Write(listViewListConsigne.SelectedItem);
                System.Diagnostics.Debug.Write(consigneTmp);
                Microsoft.Win32.OpenFileDialog openPicker = new Microsoft.Win32.OpenFileDialog();
                openPicker.DefaultExt = ".jpg";
                openPicker.Filter = "Images |*.jpg;*.jpeg; *.png";

                //var picker = new Windows.Storage.Pickers.FileOpenPicker();
                //picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
                //picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
                //picker.FileTypeFilter.Add(".jpg");
                //picker.FileTypeFilter.Add(".jpeg");
                //picker.FileTypeFilter.Add(".png");
                Nullable<bool> result = openPicker.ShowDialog();

                if (result == true)
                {
                    var imageBtm = new BitmapImage();
                    imageBtm.UriSource = new Uri(openPicker.FileName, UriKind.RelativeOrAbsolute);
                    //ListLignesConsignes[consigneTmp.id].imgBitmap = imageBtm;
                    ListLignesConsignes[consigneTmp.id].img_path = openPicker.FileName;
                    System.Diagnostics.Debug.WriteLine(openPicker.SafeFileName);
                    ListLignesConsignes[consigneTmp.id].imgNom = openPicker.SafeFileName;

                    chargerListConsignetmp();

                }
            }
            

        }

        private void nomListeConsignes_GotFocus(object sender, RoutedEventArgs e)
        {
            nomListeConsignes.Text = "";
        }

        private void listViewListConsigne_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            consigneTmp = (Consigne)(sender as ListView).SelectedItem;
        }

        private void chargerListConsignetmp()
        {

            if (ListLignesConsignes != null)
            {
                listConsignetmp = ListLignesConsignes;
            }
            listViewListConsigne.ItemsSource = null;
            listViewListConsigne.ItemsSource = listConsignetmp;
            //   System.Diagnostics.Debug.Write(ListLignesConsignes);
            System.Diagnostics.Debug.Write(listConsignetmp);
        }
        private void btn_ajoutLigneConsigne_Click(object sender, RoutedEventArgs e)
        {
            chargerListConsignetmp();
            if (listConsignetmp.Count() != 0)
            {
                index = listConsignetmp[listConsignetmp.Count - 1].id + 1;
            }
            else
            {
                index = 0;
            }

            Consigne ligneConsigne = new Consigne { id = index, description = "", img_path = "Assets/icons/addimg.png" };
            ListLignesConsignes.Add(ligneConsigne);
            index++;
        }
    }
}
