﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using consignelaWpf.Model;

namespace consignelaWpf.pages
{
    /// <summary>
    /// Interaction logic for AffichageConPresc.xaml
    /// </summary>
    public partial class AffichageConPresc : Page
    {
        public int id;
        public String imgpath ="";
        public String description = "";
        public List<Consigne> listConsignes;
        public ListConsignes listeConsignestmp;
        public int index =0 ;
        internal List<ListView> gridViewTabMedicament = new List<ListView>();
        internal Prescription prescriptionTmp = App.prescriptionSelect;
        internal PrescriptionTab prescriptionTab = App.prescriptionTabSelect;
        internal List<Medicament> listMedicament;
        internal List<Medicament> tabListMedicaments;

        public AffichageConPresc()
        {
            
            InitializeComponent();
            
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            loadListeConsigne();
            //loadGridView();

            loadPrescription();
        }
        //public void loadGridView()
        //{
        //    gridViewTabMedicament.Add(gridViewTabMedicament11);
        //    gridViewTabMedicament.Add(gridViewTabMedicament12);
        //    gridViewTabMedicament.Add(gridViewTabMedicament13);
        //    gridViewTabMedicament.Add(gridViewTabMedicament14);
        //    gridViewTabMedicament.Add(gridViewTabMedicament15);
        //    gridViewTabMedicament.Add(gridViewTabMedicament16);
        //    gridViewTabMedicament.Add(gridViewTabMedicament17);
        //    gridViewTabMedicament.Add(gridViewTabMedicament21);
        //    gridViewTabMedicament.Add(gridViewTabMedicament22);
        //    gridViewTabMedicament.Add(gridViewTabMedicament23);
        //    gridViewTabMedicament.Add(gridViewTabMedicament24);
        //    gridViewTabMedicament.Add(gridViewTabMedicament25);
        //    gridViewTabMedicament.Add(gridViewTabMedicament26);
        //    gridViewTabMedicament.Add(gridViewTabMedicament27);
        //    gridViewTabMedicament.Add(gridViewTabMedicament31);
        //    gridViewTabMedicament.Add(gridViewTabMedicament32);
        //    gridViewTabMedicament.Add(gridViewTabMedicament33);
        //    gridViewTabMedicament.Add(gridViewTabMedicament34);
        //    gridViewTabMedicament.Add(gridViewTabMedicament35);
        //    gridViewTabMedicament.Add(gridViewTabMedicament36);
        //    gridViewTabMedicament.Add(gridViewTabMedicament37);
        //}
        public void loadPrescription()
        {
            listMedicament = prescriptionTmp.medicaments;
            //tbId.Text = prescriptionTmp.nom;
            if (prescriptionTmp != null)
            {
                listMedicament = prescriptionTmp.medicaments;

                //gridViewNomsMedicaments.ItemsSource = null;
                //gridViewNomsMedicaments.ItemsSource = listMedicament;


                //for (int i = 0; i < 21; i++)
                //{
                //    if (prescriptionTab.momentJour[i].listMedicaments.Count() != 0)
                //    {
                //        tabListMedicaments = prescriptionTab.momentJour[i].listMedicaments;
                //        gridViewTabMedicament[i].ItemsSource = null;
                //        gridViewTabMedicament[i].ItemsSource = tabListMedicaments;
                //    }
                //}
            }
        }

        public void loadListeConsigne ()
        {
            listeConsignestmp = App.consignesSelect;
            listConsignes = App.consignesSelect.consignes;

            id = listeConsignestmp.consignes[index].id;
            imgpath = listeConsignestmp.consignes[index].img_path;
            description = listeConsignestmp.consignes[index].description;

            afficheConsignes(id, imgpath, description);
            backConsigne.IsEnabled = false;
        }

        public void afficheConsignes(int id,String imgpath,String description)
        {
            BitmapImage bimage = new BitmapImage();
            bimage.BeginInit();
            bimage.UriSource = new Uri(imgpath, UriKind.RelativeOrAbsolute);
            bimage.EndInit();
            imgConsigne.Source = bimage;

            tbId.Text = id.ToString();
            
            tbDescription.Text = description;
        }

   
        
        private void nextConsigne_Click(object sender, RoutedEventArgs e)
        {
            index++;
            if (index < listeConsignestmp.consignes.Count )
            {                
                afficheConsignes(listeConsignestmp.consignes[index].id, listeConsignestmp.consignes[index].img_path, listeConsignestmp.consignes[index].description);
            }   
            else 
            {
                this.NavigationService.Navigate(new Uri("pages/Pilulier.xaml", UriKind.Relative));
            }
         
        }

        private void backConsigne_Click(object sender, RoutedEventArgs e)
        {
            //if (index == listeConsignestmp.consignes.Count)
            //{
            //    gridDescription.Visibility = Visibility.Visible;
            //    imgConsigne.Visibility = Visibility.Visible;
            //    prescTabulaire.Visibility = Visibility.Collapsed;
            //    nextConsigne.IsEnabled = true;
            //}

            index--;
            
                if (index >= 0)
            {                  
                afficheConsignes(listeConsignestmp.consignes[index].id, listeConsignestmp.consignes[index].img_path, listeConsignestmp.consignes[index].description);
            }
             if(index ==0)
            {
                backConsigne.IsEnabled = false;
            }
        }
    }
}
