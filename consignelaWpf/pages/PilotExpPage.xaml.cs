﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using consignelaWpf.Model;

namespace consignelaWpf.pages
{
    /// <summary>
    /// Interaction logic for PilotExpPage.xaml
    /// </summary>
    public partial class PilotExpPage : Page
    {
        public String date;
        private String type;
        public String utilisation;
        public String pilulier;
        public String format;
        public String mode;
        public String navigation;
        public String modeFlou;
        private String codeGenere;
        internal bool nomExpGotF = false;
        internal bool numParGotF = false;
        internal bool nomParGotf = false;

        internal bool nomExpverif = false;
        internal bool numParverif = false;
        internal bool nomParverif = false;

        public PilotExpPage()
        {
            InitializeComponent();
        }

        //private void button_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Frame.Navigate(typeof(MainPage), null);
        //}

        //private void back_Click(object sender, RoutedEventArgs e)
        //{
        //    if (this.Frame.CanGoBack)
        //    {
        //        this.Frame.GoBack();
        //    }
        //}

        private void tgb_Ag_Checked(object sender, RoutedEventArgs e)
        {
            type = "AG";
            tgb_Pk.IsChecked = false;
            tgb_Co.IsChecked = false;
            tgb_Au.IsChecked = false;
        }

        private void tgb_Pk_Checked(object sender, RoutedEventArgs e)
        {
            type = "PK";
            tgb_Ag.IsChecked = false;
            tgb_Co.IsChecked = false;
            tgb_Au.IsChecked = false;
        }

        private void tgb_Co_Checked(object sender, RoutedEventArgs e)
        {
            type = "CO";
            tgb_Ag.IsChecked = false;
            tgb_Pk.IsChecked = false;
            tgb_Au.IsChecked = false;
        }

        private void tgb_Au_Checked(object sender, RoutedEventArgs e)
        {
            type = "AU";
            tgb_Ag.IsChecked = false;
            tgb_Pk.IsChecked = false;
            tgb_Co.IsChecked = false;
        }

        private void tgb_Ent_Checked(object sender, RoutedEventArgs e)
        {
            utilisation = "entr";
            tgb_Pass.IsChecked = false;
        }

        private void tgb_Pass_Checked(object sender, RoutedEventArgs e)
        {
            utilisation = "pass";
            tgb_Ent.IsChecked = false;
        }

        private void tgb_activ_Checked(object sender, RoutedEventArgs e)
        {
            navigation = "activee";
            tgb_desac.IsChecked = false;
        }

        private void tgb_desac_Checked(object sender, RoutedEventArgs e)
        {
            navigation = "desactivee";
            tgb_activ.IsChecked = false;
        }

        private void tgb_verb_Checked(object sender, RoutedEventArgs e)
        {
            format = "verb";
            tgb_tab.IsChecked = false;
        }

        private void tgb_tab_Checked(object sender, RoutedEventArgs e)
        {
            format = "tab";
            tgb_verb.IsChecked = false;
        }

        private void tgb_PilTab_Checked(object sender, RoutedEventArgs e)
        {
            pilulier = "tab";
            tgb_PilCirc.IsChecked = false;
        }

        private void tgb_PilCirc_Checked(object sender, RoutedEventArgs e)
        {
            pilulier = "circ";
            tgb_PilTab.IsChecked = false;
        }

        private void tgb_Mono_Checked(object sender, RoutedEventArgs e)
        {
            mode = "mono";
            tgb_Multi.IsChecked = false;
        }

        private void tgb_Multi_Checked(object sender, RoutedEventArgs e)
        {
            mode = "multi";
            tgb_Mono.IsChecked = false;
        }

        private void tgb_FlouActiv_Checked(object sender, RoutedEventArgs e)
        {
            modeFlou = "activee";
            tgb_FlouDesac.IsChecked = false;
        }

        private void tgb_FlouDesac_Checked(object sender, RoutedEventArgs e)
        {
            modeFlou = "desactivee";
            tgb_FlouActiv.IsChecked = false;
        }

        //private void DatePicker_DateChanged(object sender, DatePickerValueChangedEventArgs e)
        //{


        //}

        private void btn_Gen_Click(object sender, RoutedEventArgs e)
        {
            if (nomExpverif == true && numParverif == true && nomParverif == true && !String.IsNullOrEmpty(tb_age.Text) && !String.IsNullOrEmpty(type))
            {
                fieldMissing.Opacity = 0;
                codeGenere = "S" + tb_numParticipant.Text + "_" + tb_nomParticipant.Text + tb_age.Text + "_" + tb_mois.Text + type + "_" + dateExp.SelectedDate.Value.Date.ToString("dd-MM-yyyy");
                tb_codeGen.Text = codeGenere;

            }
            else
            {
                fieldMissing.Opacity = 1;
            }
        }
      

        private void expLaunch_Click(object sender, RoutedEventArgs e)
        {
            //System.Diagnostics.Debug.Write(codeGenere);
            if(!String.IsNullOrEmpty(codeGenere))
            {
                App.codeParticipant = codeGenere;
                this.NavigationService.Navigate(new Uri("pages/AffichageConPresc.xaml", UriKind.Relative));
                //framePages.Navigate(typeof(Experience), null);
                App.experimentateur = tb_nomExperimentateur.Text;
                App.date = dateExp.SelectedDate.Value.Date.ToString("dd-MMMM-yyyy");
                App.type = type;
                App.utilisation = utilisation;
                App.pilulier = pilulier;
                App.format = format;
                App.mode = mode;
                App.navigation = navigation;
                
            }
            

        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime now = DateTime.Today;
            int age, month;
            if (now.Year >= date_naiss.SelectedDate.Value.Year)
            {
                age = now.Year - date_naiss.SelectedDate.Value.Year;
                month = Math.Abs((DateTime.Today.Date.Month) - (date_naiss.SelectedDate.Value.Month));

                if (DateTime.Today.Date.Month < date_naiss.SelectedDate.Value.Month)
                {
                    age--;
                    month = 12 - Math.Abs((date_naiss.SelectedDate.Value.Month) - (DateTime.Today.Date.Month));
                }
                tb_age.Text = age.ToString();
                tb_mois.Text = month.ToString();
            }




            // tb_mois.Text = (Math.Abs((DateTime.Today.Date.Month) - (date_naiss.Date.Month))).ToString();
        }

        private void tb_nomExperimentateur_GotFocus(object sender, RoutedEventArgs e)
        {
            tb_nomExperimentateur.Text = "";
            nomExpGotF = true;
        }

        private void tb_numParticipant_GotFocus(object sender, RoutedEventArgs e)
        {
            tb_numParticipant.Text = "";
            numParGotF = true;
        }

        private void tb_nomParticipant_GotFocus(object sender, RoutedEventArgs e)
        {
            tb_nomParticipant.Text = "";
            nomParGotf = true;
        }


        private void tb_numParticipant_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (Char.IsLetter((char)e.Key))
                verifNumPar.Visibility = Visibility.Visible;
        }

        private void tb_nomParticipant_PreviewKeyDown(object sender, KeyEventArgs e)
        {


            if (Char.IsLetter((char)e.Key))
                verifNomPar.Visibility = Visibility.Visible;
        }

       

        private void tb_nomExperimentateur_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (nomExpGotF == true)
            {
                if (String.IsNullOrEmpty(tb_nomExperimentateur.Text) || System.Text.RegularExpressions.Regex.IsMatch(tb_nomExperimentateur.Text, "[^A-Z]"))
                {
                    verifNomExp.Visibility = Visibility.Visible;
                    nomExpverif = false;
                }
                else
                {
                    verifNomExp.Visibility = Visibility.Collapsed;
                    nomExpverif = true;
                }

            }

        }

        private void tb_numParticipant_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (numParGotF == true)
            {
                if (String.IsNullOrEmpty(tb_numParticipant.Text) || System.Text.RegularExpressions.Regex.IsMatch(tb_numParticipant.Text, "[^0-9]") )
                {
                    verifNumPar.Visibility = Visibility.Visible;
                    numParverif = false;
                }
                else
                {
                    verifNumPar.Visibility = Visibility.Collapsed;
                    numParverif = true;
                }

            }
        }

        private void tb_nomParticipant_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (nomParGotf == true)
            {
                if (String.IsNullOrEmpty(tb_nomParticipant.Text) || System.Text.RegularExpressions.Regex.IsMatch(tb_nomParticipant.Text, "[^A-Z]"))
                {
                    verifNomPar.Visibility = Visibility.Visible;
                    nomParverif = false;
                }
                else
                {
                    verifNomPar.Visibility = Visibility.Collapsed;
                    nomParverif = true;
                }

            }
        }
    }
}
