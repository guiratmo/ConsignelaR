﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace consignelaWpf.pages
{
    /// <summary>
    /// Interaction logic for DataPage.xaml
    /// </summary>
    public partial class DataPage : Page
    {
        string pathFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

        public class Experience
        {
            public string format { get; set; }
            public int number { get; set; }
        }
        public class Experience2
        {
            public string type { get; set; }
            public int number { get; set; }
        }
        public string FilePath, report, FileName;
        public static string jsonFromOmas, jsonOmasAvgTime, jsonOmasExpParType, jsonOmasRecupNbre;
        List<string> formatList, numberList, typeList;

        public DataPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            loadExperiences();
        }
        private void loadExperiences()
        {
            var ExcelDirectory = System.IO.Path.Combine(pathFolder, "Consignela", "Fichiers_Excel");

            List<string> ListCodeExperiences = new List<string>();
            DirectoryInfo d = new DirectoryInfo(@ExcelDirectory);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.xls"); //Getting Text files
                                                    //   string str = "";
            foreach (FileInfo file in Files)
            {

                ListCodeExperiences.Add(file.Name);
            }
            for (int i = 0; i < ListCodeExperiences.Count; i++)
            {
                System.Diagnostics.Debug.Write("\n" + ListCodeExperiences[i]);
            }
            ListViewExperiences.ItemsSource = null;
            ListViewExperiences.ItemsSource = ListCodeExperiences;

        }

        private void RecupererNbreExpParType_Click(object sender, RoutedEventArgs e)
        {
            JArray memorae_data = new JArray(new JObject(new JProperty("document-name", "brahim-experiences.json")));

            JArray pattern = new JArray(new JObject(new JProperty("type", "type-data")), new JObject(new JProperty("number", "number-data")));

            JObject JsonToOMAS = new JObject(new JProperty("memorae-data", memorae_data),

                new JProperty("action", "number-exp-patient-type"),
                new JProperty("pattern", pattern),
                 new JProperty("timeout", 200),
                 new JProperty("language", "English"),
                 new JProperty("contract-net-strategy", "take-first-answer")


                );

            System.Diagnostics.Debug.Write(JsonToOMAS.ToString());

            PostOmas("http://ace4sd.hds.utc.fr/webqueryrequest?user-input=", JsonToOMAS.ToString());

            jsonOmasRecupNbre = jsonFromOmas;
            System.Diagnostics.Debug.Write(jsonOmasRecupNbre);
            LoadBarChartDataPieSeries();
            ChartAvgTime.Visibility = Visibility.Collapsed;
            ColumnChartNbreExpParFormat.Visibility = Visibility.Collapsed;

            ChartExpParFormatPie.Visibility = Visibility.Visible;

        }

        private void ListViewExperiences_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            report = "Feuil1";
            String fileName  = (String)(sender as ListView).SelectedItem;
            FilePath = System.IO.Path.Combine(pathFolder, "Consignela", "Fichiers_Excel", fileName);
            showExcel();
        }

        public void showExcel()
        {
            string stringconn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;\";";
            System.Diagnostics.Debug.WriteLine(FilePath);
            System.Diagnostics.Debug.WriteLine(stringconn);

            OleDbConnection conn = new OleDbConnection(stringconn);
            if (FilePath != "")
            {
                OleDbDataAdapter da = new OleDbDataAdapter("Select * from [" + report + "$]", conn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                // dataGrid.ItemsSource= dt;
                dataGrid.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dt });
            }
            else
                MessageBox.Show("ER");
        }
        private void Report0_Click(object sender, RoutedEventArgs e)
        {
            report = "Feuil1";
            showExcel();
        }
        private void Report1_Click(object sender, RoutedEventArgs e)
        {
            report = "Feuil2";
            showExcel();
        }

        private void Report2_Click(object sender, RoutedEventArgs e)
        {
            report = "Feuil3";
            showExcel();
        }

        private void retour_Click(object sender, RoutedEventArgs e)
        {
            ListExperiences.Visibility = Visibility.Collapsed;
        }

        private void LoadBarChartDataPieSeries()
        {

            typeList = new List<string>();
            numberList = new List<string>();
            JArray jsonArrayFromOmas = JArray.Parse(jsonOmasRecupNbre);
            foreach (JObject o in jsonArrayFromOmas.Children<JObject>())
            {
                foreach (JProperty p in o.Properties())
                {
                    if (p.Name.Equals("type"))
                    {
                        typeList.Add(p.Value.ToString());
                    }
                    if (p.Name.Equals("number"))
                    {
                        numberList.Add(p.Value.ToString());
                    }

                }
            }
            List<Experience2> lstSource = new List<Experience2>();
            for (int i = 0; i < typeList.Count; i++)
            {
                lstSource.Add(new Experience2() { type = typeList[i], number = int.Parse(numberList[i]) });

                //  (ColumnChart.Series[0] as ColumnSeries).ItemsSource = lstSource;

            }
              (ChartExpParFormatPie.Series[0] as PieSeries).ItemsSource = lstSource;

        }

        private void showExperiences_Click(object sender, RoutedEventArgs e)
        {
            ListExperiences.Visibility = Visibility.Visible;
            afficherExperiences.Visibility = Visibility.Visible;
        }


        private void NbreExpParFormat_Click(object sender, RoutedEventArgs e)
        {
            JArray memorae_data = new JArray(
         new JObject(new JProperty("document-name", "brahim-experiences.json"))

         );

            JArray pattern = new JArray(new JObject(new JProperty("format", "format-data"), new JProperty("number", "number-data")));

            JObject JsonToOMAS = new JObject(new JProperty("memorae-data", memorae_data),

                new JProperty("action", "number-exp-format-prescrip"),
                new JProperty("pattern", pattern),
                 new JProperty("timeout", 200),
                 new JProperty("language", "English"),
                 new JProperty("contract-net-strategy", "take-first-answer")


                );

            System.Diagnostics.Debug.Write(JsonToOMAS.ToString());

            PostOmas("http://ace4sd.hds.utc.fr/webqueryrequest?user-input=", JsonToOMAS.ToString());
            jsonOmasExpParType = jsonFromOmas;
            System.Diagnostics.Debug.Write(jsonOmasExpParType);


            ChartAvgTime.Visibility = Visibility.Collapsed;

            ChartExpParFormatPie.Visibility = Visibility.Collapsed;

            ColumnChartNbreExpParFormat.Visibility = Visibility.Visible;
            LoadBarChartDataColumnSeriesExpParFormat();
        }
        public bool PostOmas(string url, string postData)
        {
            bool x = false;
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();

                //     string postData = "{\"login\":\"osamaIA03\",\"pass\":\"osamaIA03\",\"request\":\"save\"}";
                // System.Diagnostics.Debug.Write(postData);




                byte[] data = encoding.GetBytes(postData);

                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                Stream stream = request.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();
                WebResponse response = request.GetResponse();
                stream = response.GetResponseStream();

                StreamReader sr = new StreamReader(stream);
                // System.Diagnostics.Debug.Write(sr.ReadToEnd());
                // MessageBox.Show(sr.ReadToEnd());
                //  textBlockJson.Text= sr.ReadToEnd();
                jsonFromOmas = sr.ReadToEnd();

                if (jsonFromOmas.StartsWith("["))
                {
                    x = true;
                }
                else
                {
                    x = false;
                }

                sr.Close();
                stream.Close();

                return x;

            }
            catch (Exception e)
            {
                return false;
            }
        }


        public void LoadBarChartDataColumnSeriesExpParFormat()
        {


            formatList = new List<string>();
            numberList = new List<string>();
            JArray jsonArrayFromOmas = JArray.Parse(jsonOmasExpParType);

            System.Diagnostics.Debug.Write(jsonArrayFromOmas);
            foreach (JObject o in jsonArrayFromOmas.Children<JObject>())
            {
                foreach (JProperty p in o.Properties())
                {
                    if (p.Name.Equals("format"))
                    {
                        formatList.Add(p.Value.ToString());
                    }
                    if (p.Name.Equals("number"))
                    {
                        numberList.Add(p.Value.ToString());
                    }


                }
            }
            List<Experience> lstSource = new List<Experience>();
            for (int i = 0; i < formatList.Count; i++)
            {
                lstSource.Add(new Experience() { format = formatList[i], number = int.Parse(numberList[i]) });

                //  (ColumnChart.Series[0] as ColumnSeries).ItemsSource = lstSource;

            }
           (ColumnChartNbreExpParFormat.Series[0] as ColumnSeries).ItemsSource = lstSource;




        }
    }
}
