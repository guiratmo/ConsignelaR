﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace consignelaWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        String login;
        String pass, jsonFromCheckAccess;

        public MainWindow()
        {
            InitializeComponent();
            
        }
       

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            MessageBox.Show("Selection Binding");
        }

        private void experienceItem_Selected(object sender, RoutedEventArgs e)
        {
            framePages.NavigationService.Navigate(new Uri("pages/PrepareExperience.xaml", UriKind.Relative));
            //framePages.Navigate("Experience.xaml");
        }

        private void parametresItem_Selected(object sender, RoutedEventArgs e)
        {
            framePages.NavigationService.Navigate(new Uri("pages/ConfigPage.xaml", UriKind.Relative));

        }

        private void quit_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (framePages.CanGoBack){framePages.GoBack();}
        }

        private void analyseItem_Selected(object sender, RoutedEventArgs e)
        {
            framePages.NavigationService.Navigate(new Uri("pages/DataPage.xaml", UriKind.Relative));
        }

        private void main_Loaded(object sender, RoutedEventArgs e)
        {
            framePages.NavigationService.Navigate(new Uri("pages/MainPage.xaml", UriKind.Relative));
        }

        private void conexion_Click(object sender, RoutedEventArgs e)
        {
            if(!authenPopup.IsOpen) { authenPopup.IsOpen = true; }
        }

        private void connect_Click(object sender, RoutedEventArgs e)
        {
            login = login1.Text;
            pass = pass1.Password;
            bool x = false;
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();

                //     string postData = "{\"login\":\"osamaIA03\",\"pass\":\"osamaIA03\",\"request\":\"save\"}";
                // System.Diagnostics.Debug.Write(postData);



                string postData = "{\"login\":" + "\"" + login + "\"" + ",\"pass\":" + "\"" + pass + "\"" + "}";

                byte[] data = encoding.GetBytes(postData);

                WebRequest request = WebRequest.Create("http://memorae.hds.utc.fr/api/amc2/Checkaccess");
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                Stream stream = request.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();
                WebResponse response = request.GetResponse();
                stream = response.GetResponseStream();

                StreamReader sr = new StreamReader(stream);
                // System.Diagnostics.Debug.Write(sr.ReadToEnd());
                // MessageBox.Show(sr.ReadToEnd());
                //  textBlockJson.Text= sr.ReadToEnd();
                jsonFromCheckAccess = sr.ReadToEnd();
                System.Diagnostics.Debug.Write("jsonFromCheckAccess " + jsonFromCheckAccess);
                if (jsonFromCheckAccess.StartsWith("["))
                {
                    App.logIn = login1.Text;
                    App.password = pass1.Password;
                    profilNom.Text = App.logIn;
                    App.authentification = true;
                    conexion.Background = new SolidColorBrush(Color.FromArgb(255, 240, 196, 1));
                    if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }

                    x = true;
                }
                else
                {
                    x = false;
                }

                sr.Close();
                stream.Close();

                //return x;

            }
            catch (Exception ex)
            {
                //return false;
            }
        }
    }
}
