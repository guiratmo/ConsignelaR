using consignelaWpf.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace consignelaWpf.pages
{
    /// <summary>
    /// Interaction logic for Pilulier.xaml
    /// </summary>
    public partial class Pilulier : Page
    {
        internal Medicament medicamentChoisi;
        internal Prescription prescriptionTmp;
        private Prescription prescriptionTmp2;
        internal List<Medicament> listMedicament = new List<Medicament>();
        internal List<ListView> gridViewTabMedicament = new List<ListView>();
        internal List<List<Medicament>> tabListMedicaments = new List<List<Medicament>>();
        internal List<bool> open = new List<bool>();
        int j;
        bool singleTap,doubleTap, nvMedicament, mediSelected = false;
        internal String openCase = "";
        internal Medicament medicamentSelect;
        internal int indexMediSelect;
        internal int IndexgridviewSelect;


        public Pilulier()
        {
            InitializeComponent();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            chargerListMedicament();
            gridViewBoiteMedicaments.ItemsSource = listMedicament;
            loadGridView();
            loadTabListMedicaments();
            loadOpen();

            if (App.navigation == "activee")
                RetourPrescription.Visibility = Visibility.Visible;
            else if (App.navigation == "desactivee")
                RetourPrescription.Visibility = Visibility.Collapsed;
        }
        public void loadGridView()
        {
            gridViewTabMedicament.Add(gridViewTabMedicament11);
            gridViewTabMedicament.Add(gridViewTabMedicament12);
            gridViewTabMedicament.Add(gridViewTabMedicament13);
            gridViewTabMedicament.Add(gridViewTabMedicament14);
            gridViewTabMedicament.Add(gridViewTabMedicament15);
            gridViewTabMedicament.Add(gridViewTabMedicament16);
            gridViewTabMedicament.Add(gridViewTabMedicament17);
            gridViewTabMedicament.Add(gridViewTabMedicament21);
            gridViewTabMedicament.Add(gridViewTabMedicament22);
            gridViewTabMedicament.Add(gridViewTabMedicament23);
            gridViewTabMedicament.Add(gridViewTabMedicament24);
            gridViewTabMedicament.Add(gridViewTabMedicament25);
            gridViewTabMedicament.Add(gridViewTabMedicament26);
            gridViewTabMedicament.Add(gridViewTabMedicament27);
            gridViewTabMedicament.Add(gridViewTabMedicament31);
            gridViewTabMedicament.Add(gridViewTabMedicament32);
            gridViewTabMedicament.Add(gridViewTabMedicament33);
            gridViewTabMedicament.Add(gridViewTabMedicament34);
            gridViewTabMedicament.Add(gridViewTabMedicament35);
            gridViewTabMedicament.Add(gridViewTabMedicament36);
            gridViewTabMedicament.Add(gridViewTabMedicament37);
        }
        public void loadTabListMedicaments()
        {
            for (int i = 0; i < 21; i++)
            {
                tabListMedicaments.Add(new List<Medicament>());
            }

        }
        public void loadOpen()
        {
            for (int i = 0; i < 21; i++)
            {
                open.Add(false);
            }
        }
        public void chargerListMedicament()
        {
            prescriptionTmp2 = App.prescriptionSelect;
            prescriptionTmp = prescriptionTmp2;


            foreach (Medicament medicament in prescriptionTmp.medicaments)
            {
                listMedicament.Add(medicament);
            }
        }

        private void RetourPrescription_Click(object sender, RoutedEventArgs e)
        {

        }

        private void finirExp_Click(object sender, RoutedEventArgs e)
        {

        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {

        }

      
      
        //private void gridViewBoiteMedicaments_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    medicamentChoisi = (Medicament)gridViewBoiteMedicaments.SelectedItem;
        //    j = gridViewBoiteMedicaments.SelectedIndex;

        //}
        private void gridViewBoiteMedicaments_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            medicamentChoisi = (Medicament)gridViewBoiteMedicaments.SelectedItem;
            j = gridViewBoiteMedicaments.SelectedIndex;
            medicamentSelect = null;
        }
        private void ExecSingleTap(int i, ListView gridviewTab)
        {


            nvMedicament = true;
            if (open[i] && listMedicament[j].nbr > 0 && !mediSelected && medicamentChoisi != null)
            {
                System.Diagnostics.Debug.WriteLine(tabListMedicaments[i].Count);
                //if there is this medecines
                for (int k = 0; k < tabListMedicaments[i].Count; k++)
                {

                    if (tabListMedicaments[i][k].nom == medicamentChoisi.nom)
                    {

                        tabListMedicaments[i][k].nbrTab++;


                        //medicamentChoisi.nbrTab = 0;
                        nvMedicament = false;
                        break;
                    }
                }
                if (nvMedicament)
                {
                    medicamentChoisi.nbrTab = 1;
                    tabListMedicaments[i].Add(medicamentChoisi);
                }


                listMedicament[j].nbr--;

                gridViewBoiteMedicaments.ItemsSource = null;
                gridViewBoiteMedicaments.ItemsSource = listMedicament;

                gridviewTab.ItemsSource = null;
                gridviewTab.ItemsSource = tabListMedicaments[i];

                medicamentChoisi = null;
            }
            else if (listMedicament[j].nbr <= 0)
            {
                //MessageDialog boiteVide = new MessageDialog("La boite de " + listMedicament[j].nom + " est vide");
                //boiteVide.ShowAsync();
            }
            else if (medicamentChoisi == null)
            {
                //MessageDialog selesctMedi = new MessageDialog("choisissez un médicament svp");
                //selesctMedi.ShowAsync();
            }
        }
        private void ExecDoubleTap(int i, ListView gridviewTab)
        {
            if (!open[i])
            {
                if (openCase == "")
                {
                    open[i] = true;
                    openCase = gridviewTab.Name;
                    gridviewTab.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    //MessageDialog autreCaseOuverte = new MessageDialog("fermer l'autre case ouverte d'abord");
                    //autreCaseOuverte.ShowAsync();
                }
            }
            else
            {
                open[i] = false;
                openCase = "";
                gridviewTab.Background = new SolidColorBrush(Colors.WhiteSmoke);
            }

         
        }
        private void gridViewTabMedicament11_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null)
            {
                if (e.ClickCount == 1)
                {
                    ExecSingleTap(0, gridViewTabMedicament11);
                }
                else

                {
                    ExecDoubleTap(0, gridViewTabMedicament11);
                }
            }
        }
        private void gridViewTabMedicament11_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                ExecSingleTap(0, gridViewTabMedicament11);
            }
            else

            {
                ExecDoubleTap(0, gridViewTabMedicament11);
            }
        }
        //private void gridViewTabMedicament11_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    //this.doubleTap = true;
        //    this.singleTap = false;
        //    ExecDoubleTap(0, gridViewTabMedicament11);
        //}
       
        //private async void gridViewTabMedicament11_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    this.singleTap = true;
        //    await Task.Delay(200);
        //    //if (!this.doubleTap)
        //    //    this.doubleTap = true;
        //    if (this.singleTap)
        //    {
        //        ExecSingleTap(0, gridViewTabMedicament11);


        //    }
        //}

        private void gridViewTabMedicament17_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(6, gridViewTabMedicament17);
        }

        private void gridViewTabMedicament17_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(6, gridViewTabMedicament17);
        }

        private void gridViewTabMedicament21_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(7, gridViewTabMedicament21);
        }

        private void gridViewTabMedicament21_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(7, gridViewTabMedicament21);
        }

        private void gridViewTabMedicament22_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(8, gridViewTabMedicament22);
        }

        private void gridViewTabMedicament22_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(8, gridViewTabMedicament22);
        }

        private void gridViewTabMedicament23_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(9, gridViewTabMedicament23);
        }

        private void gridViewTabMedicament23_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(9, gridViewTabMedicament23);
        }

        private void gridViewTabMedicament24_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(10, gridViewTabMedicament24);
        }

        private void gridViewTabMedicament24_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(10, gridViewTabMedicament24);
        }

        private void gridViewTabMedicament25_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(11, gridViewTabMedicament25);
        }

        private void gridViewTabMedicament25_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(11, gridViewTabMedicament25);
        }

        private void gridViewTabMedicament26_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(12, gridViewTabMedicament26);
        }

        private void gridViewTabMedicament26_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(12, gridViewTabMedicament26);
        }

        private void gridViewTabMedicament27_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(13, gridViewTabMedicament27);
        }

        private void gridViewTabMedicament27_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(13, gridViewTabMedicament27);
        }

        private void gridViewTabMedicament31_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(14, gridViewTabMedicament31);
        }

        private void gridViewTabMedicament31_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(14, gridViewTabMedicament31);
        }

        private void gridViewTabMedicament32_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(15, gridViewTabMedicament32);
        }

        private void gridViewTabMedicament32_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(15, gridViewTabMedicament32);
        }

        private void gridViewTabMedicament33_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(16, gridViewTabMedicament33);
        }

        private void gridViewTabMedicament33_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(16, gridViewTabMedicament33);
        }

        private void gridViewTabMedicament34_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(17, gridViewTabMedicament34);
        }

        private void gridViewTabMedicament34_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(17, gridViewTabMedicament34);
        }

        private void gridViewTabMedicament35_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(18, gridViewTabMedicament35);
        }

        private void gridViewTabMedicament35_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(18, gridViewTabMedicament35);
        }

        private void gridViewTabMedicament36_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(19, gridViewTabMedicament36);
        }

        private void gridViewTabMedicament36_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(19, gridViewTabMedicament36);
        }

        private void gridViewTabMedicament12_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(1, gridViewTabMedicament12);
        }
     
        private void gridViewTabMedicament12_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(1, gridViewTabMedicament12);
        }
        private void gridViewTabMedicament12_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(1, gridViewTabMedicament12);
        }
        private void gridViewTabMedicament13_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(2, gridViewTabMedicament13);
        }

        private void gridViewTabMedicament13_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(2, gridViewTabMedicament13);
        }

        private void gridViewTabMedicament14_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(3, gridViewTabMedicament14);
        }

        private void gridViewTabMedicament14_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(3, gridViewTabMedicament14);
        }

        private void gridViewTabMedicament15_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(4, gridViewTabMedicament15);
        }

        private void gridViewTabMedicament15_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(4, gridViewTabMedicament15);
        }

        private void gridViewTabMedicament16_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(5, gridViewTabMedicament16);
        }

        

        private void gridViewTabMedicament16_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(5, gridViewTabMedicament16);
        }

        

        private void gridViewTabMedicament37_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(20, gridViewTabMedicament37);
        }

        private void gridViewTabMedicament37_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.singleTap = true;
            Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(20, gridViewTabMedicament37);
        }
    }
}
